package com.example.stepcounter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private final String TAG = getClass().getName();

    private Sensor accelerometer;
    private SensorManager sensorManager;

    private TextView countView;
    private DonutProgress donutView;

    private float filteredAcc = 0;
    private boolean isFinished = false;
    private int count;
    private final int maxCount = 10;
    private final float alpha = 0.1f;
    private final float threshold = 12f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countView = findViewById(R.id.count);
        donutView = findViewById(R.id.donut_progress);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onStart() {
        super.onStart();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onStop() {
        super.onStop();
        sensorManager.unregisterListener(this);
    }

    public void onReset(View view) {
        count = 0;
        isFinished = false;
        filteredAcc = 0;
        countView.setText("0");
        donutView.setDonut_progress("0");
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (!isFinished) {
            // Get size of acceleration
            float[] values = event.values;
            float X = values[0], Y = values[1], Z = values[2];
            float acc = (float) Math.sqrt(X * X + Y * Y + Z * Z);

            // Exponential Moving Average
            // https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average
            float filteredPrevAcc;
            if (filteredAcc == 0)
                filteredPrevAcc = acc;
            else
                filteredPrevAcc = alpha * acc + (1 - alpha) * filteredAcc;

            // Log filtered acceleration
            Log.d(TAG, String.valueOf(filteredPrevAcc));

            // Simple Peak Detection
            if (filteredAcc < threshold && threshold < filteredPrevAcc) {
                count++;
                countView.setText(String.valueOf(count));
                donutView.setDonut_progress(String.valueOf(100 * count / maxCount));
                if (count == maxCount)
                    isFinished = true;
            }
            filteredAcc = filteredPrevAcc;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }
}