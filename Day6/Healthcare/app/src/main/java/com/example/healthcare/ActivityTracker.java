package com.example.healthcare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.healthcare.ml.Har;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Locale;

import app.futured.donut.DonutProgressView;
import app.futured.donut.DonutSection;

public class ActivityTracker extends AppCompatActivity implements SensorEventListener {
    private static final String TAG = ActivityTracker.class.getName();

    private TextView activityView;
    private DonutProgressView donutView;

    private Sensor accelerometer;
    private SensorManager sensorManager;

    private final int maxCounts = 100;
    private final String[] activities = { "Standing", "Walking", "Running" };
    private final int[] colors = { Color.BLUE, Color.YELLOW, Color.GREEN };
    private int[] activityCounts;
    private DonutSection[] donutSections;
    private TextView[] activityTextViews;

    private final int numAxes = 3;
    private final int numSamples = 100;
    private final float normalizeValue = 9.8f;

    private int sampleIndex = 0;
    private float[] sensorValues;
    private ByteBuffer inputBuffer;

    private Har model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);

        activityView = findViewById(R.id.activity);
        donutView = findViewById(R.id.donut_view);
        activityTextViews = new TextView[3];
        activityTextViews[0] = findViewById(R.id.standing);
        activityTextViews[1] = findViewById(R.id.walking);
        activityTextViews[2] = findViewById(R.id.running);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        inputBuffer = ByteBuffer.allocateDirect(numSamples * numAxes * 4);
        inputBuffer.order(ByteOrder.nativeOrder());
        sensorValues = new float[numSamples * numAxes];
        activityCounts = new int[activities.length];
        donutSections = new DonutSection[activities.length];
    }

    private void syncDonuts() {
        // Sync Donut
        for (int activityId = 0; activityId < activities.length; activityId++) {
            donutSections[activityId] = new DonutSection(
                    "section_" + activityId,
                    colors[activityId],
                    activityCounts[activityId]
            );
        }
        donutView.setCap(maxCounts);
        donutView.submitData(Arrays.asList(donutSections));

        // Sync Texts
        for (int activityId = 0; activityId < activities.length; activityId++) {
            int count = activityCounts[activityId];
            String activity = activities[activityId];
            activityTextViews[activityId].setText(String.format(Locale.getDefault(), "%s: %ds", activity, 2 * count));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            model = Har.newInstance(getApplicationContext());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onStop() {
        super.onStop();
        sensorManager.unregisterListener(this);
        if (model != null) {
            model.close();
        }
    }

    private int inference() {
        if (model == null)
            return -1;

        inputBuffer.asFloatBuffer().put(sensorValues);

        // Creates inputs for reference.
        TensorBuffer inputFeature0 = TensorBuffer.createFixedSize(new int[]{1, 100, 3, 1}, DataType.FLOAT32);
        inputFeature0.loadBuffer(inputBuffer);

        // Runs model inference and gets result.
        Har.Outputs outputs = model.process(inputFeature0);
        TensorBuffer outputFeature0 = outputs.getOutputFeature0AsTensorBuffer();

        return argmax(outputFeature0.getFloatArray());
    }

    private int argmax(float[] arr) {
        float max = 0;
        int maxIdx = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                maxIdx = i;
            }
        }
        return maxIdx;
    }

    public void onReset(View view) {
        sampleIndex = 0;
        activityView.setText(R.string.activity);
        Arrays.fill(activityCounts, 0);
        syncDonuts();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        for (int i = 0; i < numAxes; i++) {
            sensorValues[sampleIndex * numAxes + i] = event.values[i] / normalizeValue;
        }
        sampleIndex++;
        if (sampleIndex == numSamples) {
            sampleIndex = 0;
            int activityId = inference();
            if (0 <= activityId && activityId < 3) {
                activityView.setText(activities[activityId]);
                activityCounts[activityId]++;
                syncDonuts();
            } else {
                activityView.setText(R.string.error);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }
}