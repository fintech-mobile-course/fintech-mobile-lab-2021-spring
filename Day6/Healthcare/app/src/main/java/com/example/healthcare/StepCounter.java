package com.example.healthcare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;

public class StepCounter extends AppCompatActivity implements SensorEventListener {
    private static final String TAG = StepCounter.class.getName();

    private TextView countView;
    private DonutProgress donutView;

    private Sensor accelerometer;
    private SensorManager sensorManager;

    private boolean isStopped = false;
    private final int targetCount = 100;
    private int count = 0;
    private float prev = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_counter);

        countView = findViewById(R.id.count);
        donutView = findViewById(R.id.donut_progress);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onStart() {
        super.onStart();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onStop() {
        super.onStop();
        sensorManager.unregisterListener(this);
    }

    public void onReset(View view) {
        isStopped = false;
        count = 0;
        prev = 0;
        countView.setText(R.string.count);
        donutView.setDonut_progress("0");
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (isStopped)
            return;
        float smoothing_factor = 0.1f, threshold = 12f;
        float raw = (float) Math.sqrt(event.values[0] * event.values[0] + event.values[1] * event.values[1] + event.values[2] * event.values[2]);
        float curr = prev == 0 ? raw : smoothing_factor * raw + (1 - smoothing_factor) * prev;
        if (prev < threshold && threshold < curr) {
            count++;
            countView.setText(String.format("Count: %d", count));
            donutView.setDonut_progress(String.valueOf(100 * count / targetCount));
            if (count >= targetCount) {
                isStopped = true;
            }
        }
        prev = curr;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }
}