package com.example.healthcare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onActivityTrackerClick(View view) {
        Intent intent = new Intent(this, ActivityTracker.class);
        startActivity(intent);
    }

    public void onStepCounterClick(View view) {
        Intent intent = new Intent(this, StepCounter.class);
        startActivity(intent);
    }
}